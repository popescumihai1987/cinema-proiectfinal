
package gui;

import controller.ClientController;
import db.Contact;
import db.Difuzare;
import db.Film;
import db.Sala;
import db.Spectator;
import db.Tip;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.JOptionPane;


public class MainFrame extends javax.swing.JFrame {

    public MainFrame() {        
        initComponents();
        this.refreshComboFilme();
        this.refreshComboSali();
        this.refreshListaDifuzari();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        filmeComboBox = new javax.swing.JComboBox<>();
        titluLabel = new javax.swing.JLabel();
        titluTextField = new javax.swing.JTextField();
        actoriLabel = new javax.swing.JLabel();
        durataTextField = new javax.swing.JTextField();
        durataLabel = new javax.swing.JLabel();
        tipLabel = new javax.swing.JLabel();
        tipComboBox = new javax.swing.JComboBox<>();
        adaugaFilmButton = new javax.swing.JButton();
        listaFilmeLabel = new javax.swing.JLabel();
        stergeFilmButton = new javax.swing.JButton();
        actoriScrollPane = new javax.swing.JScrollPane();
        actoriTextArea = new javax.swing.JTextArea();
        listaDifuzariLabel = new javax.swing.JLabel();
        sectiuneaFilmeLabel = new javax.swing.JLabel();
        sectiuneProgramLabel = new javax.swing.JLabel();
        salaLabel = new javax.swing.JLabel();
        saliComboBox = new javax.swing.JComboBox<>();
        dataDifuzariiLabel = new javax.swing.JLabel();
        dataDifuzariiTextField = new javax.swing.JTextField();
        oraDifuzariiLabel = new javax.swing.JLabel();
        oraDifuzariiTextField = new javax.swing.JTextField();
        adaugaDifuzareButton = new javax.swing.JButton();
        sectiuneaBileteLabel = new javax.swing.JLabel();
        numeClientLabel = new javax.swing.JLabel();
        numeClientTextField = new javax.swing.JTextField();
        emailClientLabel = new javax.swing.JLabel();
        emailClientTextField = new javax.swing.JTextField();
        telClientLabel = new javax.swing.JLabel();
        telClientTextField = new javax.swing.JTextField();
        numarBileteLabel = new javax.swing.JLabel();
        numarBileteTextField = new javax.swing.JTextField();
        cumparaBileteButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        listaDifuzariScrollPane = new javax.swing.JScrollPane();
        listaDifuzari = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addHierarchyListener(new java.awt.event.HierarchyListener() {
            public void hierarchyChanged(java.awt.event.HierarchyEvent evt) {
                formHierarchyChanged(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        filmeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {  }));
        getContentPane().add(filmeComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(94, 277, 80, -1));

        titluLabel.setText("Titlu:");
        getContentPane().add(titluLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        titluTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                titluTextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(titluTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(62, 27, 60, -1));

        actoriLabel.setText("Actori:");
        getContentPane().add(actoriLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 47, -1, -1));

        durataTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                durataTextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(durataTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(108, 168, 41, -1));

        durataLabel.setText("Durata(minute):");
        getContentPane().add(durataLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 171, -1, -1));

        tipLabel.setText("Tip:");
        getContentPane().add(tipLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 197, -1, -1));

        tipComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "COMEDIE", "DRAMA", "HORROR", "TRAGIC" }));
        getContentPane().add(tipComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(42, 194, 110, -1));

        adaugaFilmButton.setText("ADAUGA FILM");
        adaugaFilmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adaugaFilmButtonActionPerformed(evt);
            }
        });
        getContentPane().add(adaugaFilmButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 225, -1, -1));

        listaFilmeLabel.setText("LISTA FILME:");
        getContentPane().add(listaFilmeLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, -1, -1));

        stergeFilmButton.setText("STERGE FILM");
        stergeFilmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stergeFilmButtonActionPerformed(evt);
            }
        });
        getContentPane().add(stergeFilmButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 310, -1, -1));

        actoriScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        actoriTextArea.setColumns(20);
        actoriTextArea.setRows(5);
        actoriScrollPane.setViewportView(actoriTextArea);

        getContentPane().add(actoriScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 50, 102, 87));

        listaDifuzariLabel.setText("LISTA DIFUZARI:");
        getContentPane().add(listaDifuzariLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(216, 254, -1, -1));

        sectiuneaFilmeLabel.setText("SECTIUNEA FILME");
        getContentPane().add(sectiuneaFilmeLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 144, 21));

        sectiuneProgramLabel.setText("SECTIUNEA PROGRAM");
        getContentPane().add(sectiuneProgramLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(216, 56, 127, 18));

        salaLabel.setText("Sala:");
        getContentPane().add(salaLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(216, 88, -1, -1));

        saliComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {  }));
        getContentPane().add(saliComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 85, 134, -1));

        dataDifuzariiLabel.setText("Data difuzarii:");
        getContentPane().add(dataDifuzariiLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 140, -1, -1));

        dataDifuzariiTextField.setToolTipText("ZZ/LL/AAAA");
        dataDifuzariiTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dataDifuzariiTextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(dataDifuzariiTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 160, 90, -1));

        oraDifuzariiLabel.setText("Ora difuzarii:");
        getContentPane().add(oraDifuzariiLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 140, -1, -1));
        getContentPane().add(oraDifuzariiTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 160, 70, -1));

        adaugaDifuzareButton.setText("ADAUGA DIFUZARE");
        adaugaDifuzareButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adaugaDifuzareButtonActionPerformed(evt);
            }
        });
        getContentPane().add(adaugaDifuzareButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 220, 160, -1));

        sectiuneaBileteLabel.setText("SECTIUNEA BILETE:");
        getContentPane().add(sectiuneaBileteLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, -1, -1));

        numeClientLabel.setText("Nume:");
        getContentPane().add(numeClientLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 30, -1, -1));
        getContentPane().add(numeClientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 50, 130, -1));

        emailClientLabel.setText("e-mail:");
        getContentPane().add(emailClientLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 80, -1, -1));
        getContentPane().add(emailClientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 100, 130, -1));

        telClientLabel.setText("Telefon:");
        getContentPane().add(telClientLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 130, -1, -1));

        telClientTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telClientTextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(telClientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 150, 130, -1));

        numarBileteLabel.setText("Numar bilete:");
        getContentPane().add(numarBileteLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 180, -1, -1));
        getContentPane().add(numarBileteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 180, 60, -1));

        cumparaBileteButton.setText("CUMPARA BILETE");
        cumparaBileteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cumparaBileteButtonActionPerformed(evt);
            }
        });
        getContentPane().add(cumparaBileteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 210, -1, -1));

        jLabel1.setText("ex: 01/01/2000");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 190, -1, -1));

        jLabel2.setText("ex: 15:20");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, -1, -1));

        listaDifuzari.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listaDifuzariScrollPane.setViewportView(listaDifuzari);

        getContentPane().add(listaDifuzariScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 280, 390, 140));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void titluTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_titluTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_titluTextFieldActionPerformed

    private void formHierarchyChanged(java.awt.event.HierarchyEvent evt) {//GEN-FIRST:event_formHierarchyChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_formHierarchyChanged

    private void dataDifuzariiTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dataDifuzariiTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dataDifuzariiTextFieldActionPerformed

    private void telClientTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telClientTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_telClientTextFieldActionPerformed

    private void adaugaFilmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adaugaFilmButtonActionPerformed
        this.adaugareFilm(titluTextField.getText(),
                durataTextField.getText(),
                tipComboBox.getSelectedItem().toString(),
                actoriTextArea.getText());
    }//GEN-LAST:event_adaugaFilmButtonActionPerformed

    private void refreshCampuriFilm(){
        List<Film> filme = ClientController.getInstance().gasesteFilm(filmeComboBox.getSelectedItem().toString());
        titluTextField.setText(filme.get(0).getTitlu());
        String durata = String.valueOf(filme.get(0).getDurata());
        durataTextField.setText(durata);
        List<String> actori = filme.get(0).getActori();
        String actoriString = actori.stream().map(s -> s.substring(0, 1))
                                .collect(Collectors.joining(", "));        
    }
    
    private void refreshListaDifuzari(){
        List<Difuzare> difuzari = ClientController.getInstance().getDifuzari();
        DefaultListModel<String> dml= new DefaultListModel<>();
        List<Sala> sali = ClientController.getInstance().getSali();
        for (int i = 0; i < difuzari.size(); i++) {
            Sala sala = new Sala();
            for (int j = 0; j < sali.size(); j++) {
                if (sali.get(j).getNume().equalsIgnoreCase
                            (difuzari.get(i).getSala().getNume())){
                    sala = sali.get(j);
                    System.out.println(sala.getNumarLocuri());
                    break;
                }
            }
            System.out.println(sala.getNumarLocuri());
            dml.add(i, showDate(difuzari.get(i).getData()) +
                    "   " + difuzari.get(i).toString() +
                    "   Bilete ramase: " +
                    (sala.getNumarLocuri() -
                            difuzari.get(i).getBilete().size()));
        }
        JList jlist = new JList(dml);
        listaDifuzari.setModel(dml);                
    }
    
    private String showDate(Date date){
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String data = formatter.format(date);
        return data;
    }
    
    private void cumparaBilete(String nume, String email, String tel, int n) throws ParseException, NullPointerException{
        if (listaDifuzari.getSelectedValue()==null){
            JOptionPane.showMessageDialog(rootPane, "Selectioneaza o difuzare din lista!!");
            return;
        }
        String ddd = listaDifuzari.getSelectedValue();
        String date = ddd.substring(0, 10);
        String hour = ddd.substring(11, 16);
        String[] split1 = listaDifuzari.getSelectedValue().toString().split(":");
        String[] split2 = split1[3].split(" ");
        String sala = split2[1];        
        List<Sala> listaSali = ClientController.getInstance().findSala(sala);
        Sala salaObj = listaSali.get(0);
        Date data = processDate(date, hour);   
        Difuzare d = ClientController.getInstance().gasesteDifuzare(data, salaObj);
        System.out.println(d);
        if (d.getSala().getNumarLocuri() < d.getBilete().size() + n){
            JOptionPane.showMessageDialog(rootPane, "Sala mai are doar " + (d.getSala().getNumarLocuri() - d.getBilete().size()) + " locuri disponibile!!!!");
            return;
        }
        Spectator s = new Spectator();
        s.setNume(nume);
        Contact c = new Contact();
        c.setEmail(email);
        c.setTelefon(tel);
        s.setContact(c);
        s.setNrBilete(n);
        List<Spectator> bilete = d.getBilete();
        for (int i = 0; i < n; i++) {
            String spec = new String(nume+i);            
            bilete.add(new Spectator(spec));
        }
        System.out.println(bilete.size());
        d.setBilete(bilete);
        System.out.println(bilete.size());
        ClientController.getInstance().adaugaSpectator(d);
        this.refreshListaDifuzari();
    }
    
    private void adaugaDifuzare(String salaIn, String d, String h, String f) throws ParseException {
        Date data = null;
        if (this.processDate(d, h) == null) {
            return;
        }
        data = this.processDate(d, h);
        List<Sala> sali = ClientController.getInstance().findSala(saliComboBox.getSelectedItem().toString());
        Sala s = sali.get(0);
        try{
            Difuzare dif = ClientController.getInstance().gasesteDifuzare(data, s);
            if(dif != null){
                JOptionPane.showMessageDialog(rootPane, "Aceasta difuzare exista deja!!!!");
                return;
            }
        } catch (NoResultException e) {
            System.out.println("Prima inregistrare");
        }
        List<Film> filme = ClientController.getInstance().gasesteFilm(filmeComboBox.getSelectedItem().toString());
        Film film = filme.get(0);
        Difuzare difuzare = new Difuzare();
        difuzare.setData(data);
        difuzare.setFilmDifuzat(film);
        difuzare.setSala(s);
        ClientController.getInstance().adaugaDifuzare(difuzare);
        this.refreshListaDifuzari();
    }
    
    private Date processDate(String d, String h) throws ParseException {
        if (!h.matches("\\d{2}:\\d{2}")) {
            JOptionPane.showMessageDialog(rootPane, "Ora are formatul gresit, vezi exemplul!!!!");
            return null;
        }
        else if (!d.matches("\\d{2}/\\d{2}/\\d{4}")) {
            JOptionPane.showMessageDialog(rootPane, "Data are formatul gresit, vezi exemplul!!!!");
            return null;
        }
        String toParseDate = d + " " + h;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date data = formatter.parse(toParseDate);
        return data;
    }
    
    private void durataTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_durataTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_durataTextFieldActionPerformed

    private void stergeFilmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stergeFilmButtonActionPerformed
        this.findAndDeleteFilm(filmeComboBox.getSelectedItem().toString());
        System.out.println(filmeComboBox.getSelectedItem().toString());
        this.refreshComboFilme();
        this.refreshListaDifuzari();
    }//GEN-LAST:event_stergeFilmButtonActionPerformed

    private void adaugaDifuzareButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adaugaDifuzareButtonActionPerformed
        try {
            this.adaugaDifuzare(saliComboBox.getSelectedItem().toString(),
                    dataDifuzariiTextField.getText(),
                    oraDifuzariiTextField.getText(),
                    filmeComboBox.getSelectedItem().toString());
        } catch (ParseException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_adaugaDifuzareButtonActionPerformed

    private void cumparaBileteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cumparaBileteButtonActionPerformed
        int numarbilete = Integer.parseInt(numarBileteTextField.getText());
        
        try {
            this.cumparaBilete(numeClientTextField.getText(), emailClientTextField.getText(),
                    telClientTextField.getText(), numarbilete);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(rootPane, "Data sau ora au formatul gresit!!!");
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cumparaBileteButtonActionPerformed

    private void adaugareFilm(String titlu, String durata,
            String tip, String actori){
        if(ClientController.getInstance().gasesteFilm(titlu).size()>0){
            JOptionPane.showMessageDialog(rootPane, "Acest film exista deja!!!!");
            return;
        }       
        Film f = new Film();
        f.setTitlu(titlu);
        int d = new Integer(durata);
        f.setDurata(d);        
        f.setTip(Tip.valueOf(tip));
        List<String> actoriList = Arrays.asList(actori.split("\\s*,\\s*"));        
        f.setActori(actoriList);
        ClientController.getInstance().adaugaFilm(f);
        this.refreshComboFilme();
    }

    public void refreshComboFilme(){
        List<Film> filme = ClientController.getInstance().getFilme();
        DefaultComboBoxModel dml= new DefaultComboBoxModel();
        for (int i = 0; i < filme.size(); i++) {
            dml.addElement(filme.get(i).getTitlu());
        }
        filmeComboBox.setModel(dml);
    }
    
    public void refreshComboSali(){
        List<Sala> sali = ClientController.getInstance().getSali();
        DefaultComboBoxModel dml= new DefaultComboBoxModel();
        for (int i = 0; i < sali.size(); i++) {
            dml.addElement(sali.get(i).getNume());
        }
        saliComboBox.setModel(dml);
    }
    
    public void findAndDeleteFilm(String titlu){
        ClientController.getInstance().findAndDeleteFilm(titlu);
    }
    
    public List<Film> findFilm(String titlu){
        return ClientController.getInstance().gasesteFilm(titlu);
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel actoriLabel;
    private javax.swing.JScrollPane actoriScrollPane;
    private javax.swing.JTextArea actoriTextArea;
    private javax.swing.JButton adaugaDifuzareButton;
    private javax.swing.JButton adaugaFilmButton;
    private javax.swing.JButton cumparaBileteButton;
    private javax.swing.JLabel dataDifuzariiLabel;
    private javax.swing.JTextField dataDifuzariiTextField;
    private javax.swing.JLabel durataLabel;
    private javax.swing.JTextField durataTextField;
    private javax.swing.JLabel emailClientLabel;
    private javax.swing.JTextField emailClientTextField;
    private javax.swing.JComboBox<String> filmeComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> listaDifuzari;
    private javax.swing.JLabel listaDifuzariLabel;
    private javax.swing.JScrollPane listaDifuzariScrollPane;
    private javax.swing.JLabel listaFilmeLabel;
    private javax.swing.JLabel numarBileteLabel;
    private javax.swing.JTextField numarBileteTextField;
    private javax.swing.JLabel numeClientLabel;
    private javax.swing.JTextField numeClientTextField;
    private javax.swing.JLabel oraDifuzariiLabel;
    private javax.swing.JTextField oraDifuzariiTextField;
    private javax.swing.JLabel salaLabel;
    private javax.swing.JComboBox<String> saliComboBox;
    private javax.swing.JLabel sectiuneProgramLabel;
    private javax.swing.JLabel sectiuneaBileteLabel;
    private javax.swing.JLabel sectiuneaFilmeLabel;
    private javax.swing.JButton stergeFilmButton;
    private javax.swing.JLabel telClientLabel;
    private javax.swing.JTextField telClientTextField;
    private javax.swing.JComboBox<String> tipComboBox;
    private javax.swing.JLabel tipLabel;
    private javax.swing.JLabel titluLabel;
    private javax.swing.JTextField titluTextField;
    // End of variables declaration//GEN-END:variables
}
