
package client;

import controller.ClientController;
import db.Contact;
import db.Difuzare;
import db.Film;
import db.Sala;
import db.Spectator;
import db.Tip;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProiectfinalClient {

    public static void main(String[] args) throws ParseException, SQLIntegrityConstraintViolationException {
        Film f = new Film();
        List<String> a = new ArrayList<String>();
        a.add("Vasile Vasile");
        f.setActori(a);
        f.setDurata(120);
        f.setTip(Tip.TRAGIC);
        f.setTitlu("Filmul lui Vasile");
        SimpleDateFormat ft = new SimpleDateFormat ("dd-MM-yyyy");
        String data = "01-10-2019";
        Date t = null;
        try {         
         t = ft.parse(data);
         System.out.println(t); 
        } catch (ParseException e) {
         System.out.println("Unparseable using " + ft); 
        }
        Difuzare d = new Difuzare();
        d.setData(t);
        d.setId(111);
        d.setFilmDifuzat(f);
        Sala s = new Sala();
        s.setNumarLocuri(10);
        List<Difuzare> lista = new ArrayList<Difuzare>();
        s.setDifuzari(lista);
        s.setNume("Chichita");
        d.setSala(s);
        lista.add(d);
        f.setDifuzari(lista);
        ClientController.getInstance().adaugaDifuzare(d);
        Spectator spec = new Spectator();
        Spectator spec2 = new Spectator();
        Contact c = new Contact();
        c.setEmail("vasilicasemail@ceva.com");
        c.setTelefon("07444444444");
        spec.setContact(c);
        spec.setNume("Tot Vasilica");
        spec2.setContact(c);
        spec2.setNume("Un alt vasilica");
        List<Spectator> bilete = new ArrayList<Spectator>();
        bilete.add(spec);
        bilete.add(spec2);
        Difuzare d2 = new Difuzare();
        
        d2 = ClientController.getInstance().getDifuzare(111);
        d2.setBilete(bilete);
        ClientController.getInstance().adaugaSpectator(d2);
        
        
    }
    
}
