/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.Difuzare;
import db.Film;
import db.Sala;
import db.Spectator;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import rmi.RemoteService;

/**
 *
 * @author Mihai2.Popescu
 */
public class ClientController {
    private RemoteService serv;
    
    private ClientController(){
        try{
            Registry reg = LocateRegistry.getRegistry(7001);
            serv = (RemoteService) reg.lookup("server");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private static final class SingletonHolder{
        private static final ClientController SINGLETON = new ClientController();
    }
    
    public static ClientController getInstance(){
        return SingletonHolder.SINGLETON;
    }
    
    public List<Film> getFilme(){
        try{
            return serv.getFilme();
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void adaugaFilm(Film f){
        try{
            serv.adaugaFilm(f);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public List<Film> gasesteFilm(String titlu){
        try{
            return serv.gasesteFilm(titlu);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void findAndDeleteFilm(String titlu){
        try{
            serv.findAndDeleteFilm(titlu);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public List<Difuzare> getDifuzari(){
        try{
            return serv.getDifuzari();
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public Difuzare gasesteDifuzare(Date data, Sala sala){
        try{
            return serv.gasesteDifuzare(data, sala);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public Difuzare getDifuzare(int d){
        try{
            return serv.getDifuzare(d);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public int adaugaDifuzare(Difuzare d) {
        try{
            serv.adaugaDifuzare(d);
        }catch(RemoteException | SQLIntegrityConstraintViolationException e){
            return 1;
        }
        return 0;
    }
    
    public List<Sala> getSali(){
        try{
            return serv.getSali();
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void adaugaSala(Sala s){
        try{
            serv.adaugaSala(s);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public List<Sala> findSala(String n){
        try{
            return serv.findSala(n);
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public List<Spectator> getSpectatori(){
        try{
            return serv.getSpectatori();
        }catch(Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void adaugaSpectator(Difuzare d){
        try{
            serv.adaugaSpectator(d);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
