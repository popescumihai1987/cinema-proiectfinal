Proiectul Cinema-proiectfinal este tema propusa pentru cursul "Java 2" - Telecom Academy
Acesta contine un proiect-server, un proiect-client si un proiect-lib comun de legatura.
Pentru pornirea aplicatiei-server se va rula fisierul ProiectFinalServer.java din pachetul server al proiectului proiectfinal-server.
Pentru pornirea aplicatiei-client(gui) se va rula fisierul MainFrame.java din pachetul gui al proiectului proiectfinal-client.

Avem 4 entitati: Film, Difuzare, Sala si Spectator.
Avem "hardcoded" 3 sali de cinema la initializarea bazei de date, in momentul pornirii proiectului-server.
Cele 3 sali au 50, 75 si 100 de locuri.

Pentru introducerea unui Film se vor completa campurile Titlu, Actori(separati cu virgule) si durata si se va apasa butonul de adaugare film.
Pentru introducerea unei difuzari trebuie sa avem cel putin un Film si o Sala introduse.
Data si ora se vor scrie in formatul aratat de catre aplicatie altfel se va primi o eroare.
Dupa introducerea Difuzarii se pot cumpara bilete prin completarea datelor unui client si a numarului de bilete dorite.
Inainte de apasarea butonului pentru cumparare trebuie selectionata si o difuzare din lista astfel se va afisa o eraoare.
Se va putea observa numarul de bilete ramase la o anumita difuzare si nu se vor cumpara bilete peste aceasta valoare.

O ultima functie de baza este stergerea unui Film, stergere care va elimina din baza de date si eventualele difuzari asociate si clienti inregistrati cu bilete cumparate.
