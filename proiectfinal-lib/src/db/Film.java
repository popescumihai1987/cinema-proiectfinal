
package db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Film implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false,
            unique = true)
    private int id;
    
    @Column(name = "Titlu", nullable = false, unique = true)
    private String titlu;
    
    @Column(name = "Durata", nullable = true)
    private int durata;
    
    @Enumerated(EnumType.STRING)
    private Tip tip;
    
    @ElementCollection(fetch = FetchType.LAZY)
    @JoinTable(name = "Actori")
    private List<String> actori;
    
    @OneToMany(mappedBy = "filmDifuzat", fetch = FetchType.EAGER,
            targetEntity = Difuzare.class)
    private List<Difuzare> difuzari;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public Tip getTip() {
        return tip;
    }

    public void setTip(Tip tip) {
        this.tip = tip;
    }

    public List<String> getActori() {
        return actori;
    }

    public void setActori(List<String> actori) {
        this.actori = actori;
    }

    public List<Difuzare> getDifuzari() {
        return difuzari;
    }

    public void setDifuzari(List<Difuzare> difuzari) {
        this.difuzari = difuzari;
    }

    
    
}
