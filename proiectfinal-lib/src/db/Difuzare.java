
package db;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints={
    @UniqueConstraint(columnNames = {"data", "sala_id"})})
public class Difuzare implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Film")
    private Film filmDifuzat;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Sala sala;    

    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Spectator> bilete;

    public List<Spectator> getBilete() {
        return bilete;
    }

    public void setBilete(List<Spectator> bilete) {
        this.bilete = bilete;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Film getFilmDifuzat() {
        return filmDifuzat;
    }

    public void setFilmDifuzat(Film filmDifuzat) {
        this.filmDifuzat = filmDifuzat;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Film: " + filmDifuzat.getTitlu() + "   Sala: " + sala.getNume();
    }    
}
