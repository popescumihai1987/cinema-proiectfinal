
package rmi;

import db.Difuzare;
import db.Film;
import db.Sala;
import db.Spectator;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;


public interface RemoteService extends Remote {
    
    public List<Film> getFilme() throws RemoteException;
    
    public void adaugaFilm(Film f) throws RemoteException;
    
    public List<Film> gasesteFilm(String titlu) throws RemoteException;
    
    public void findAndDeleteFilm(String titlu) throws RemoteException;
    
    public List<Difuzare> getDifuzari() throws RemoteException;
    
    public void adaugaDifuzare(Difuzare d) throws RemoteException, SQLIntegrityConstraintViolationException;
    
    public Difuzare getDifuzare(int d) throws RemoteException;
    
    public Difuzare gasesteDifuzare(Date data, Sala sala) throws RemoteException;
    
    public List<Sala> getSali() throws RemoteException;
    
    public void adaugaSala(Sala s) throws RemoteException;
    
    public List<Sala> findSala(String n) throws RemoteException;
    
    public List<Spectator> getSpectatori() throws RemoteException;
    
    public void adaugaSpectator(Difuzare d) throws RemoteException;
    
}
