
package dao;

import db.Film;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;


public class FilmDao {
    
    private EntityManager em;
    
    public FilmDao (EntityManager em){
        this.em = em;
    }
    
    public List<Film> getFilme(){
        String sql = "SELECT f FROM Film f";
        TypedQuery<Film> query = em.createQuery(sql, Film.class);
        return query.getResultList();
    }
    
    public void adaugaFilm(Film f){
        em.persist(f);
    }
    
    public void stergeFilm(Film f){
        em.remove(f);
    }
    
    public List<Film> gasesteFilm(String titlu){
        String jpql = "SELECT f FROM Film f WHERE f.titlu = '" + titlu + "'";
        List<Film> query = em.createQuery(jpql, Film.class).getResultList();
        return query;
    }
    
}
