
package dao;

import db.Difuzare;
import db.Spectator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;


public class SpectatorDao {
    
    private EntityManager em;
    
    public SpectatorDao (EntityManager em){
        this.em = em;
    }
    
    public List<Spectator> getSpectatori(){
        String sql = "SELECT s FROM Spectator s";
        TypedQuery<Spectator> query = em.createQuery(sql, Spectator.class);
        return query.getResultList();
    }
    
}