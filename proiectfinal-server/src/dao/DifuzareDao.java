
package dao;

import db.Difuzare;
import db.Film;
import db.Sala;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


public class DifuzareDao {
    
    private EntityManager em;
    
    public DifuzareDao (EntityManager em){
        this.em = em;
    }
    
    public List<Difuzare> getDifuzari(){
        String sql = "SELECT d FROM Difuzare d";
        TypedQuery<Difuzare> query = em.createQuery(sql, Difuzare.class);
        return query.getResultList();
    }
    
    public void adaugaDifuzare(Difuzare d){
        em.merge(d);
    }
    
    public Difuzare getDifuzare(int d){
        Difuzare found = em.find(Difuzare.class, d);
        return found;
    }
    
    public void adaugaSpectator(Difuzare d){
        em.merge(d);
    }
    
    public Difuzare gasesteDifuzare(Date data, Sala sala){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String data1 = formatter.format(data);
        System.out.println(data1);
        String jpql = "select d from Difuzare d, Sala s where d.data = '" +
                data1 + "' and s.id = " + sala.getId();
        System.out.println(jpql);
        try{
            Difuzare query = em.createQuery(jpql, Difuzare.class).getSingleResult();
            return query;
        } catch (Exception e){
            return null;
        }        
    }
    
    public void stergeDifuzariDupaFilm(Film film){
          int n = em.createQuery("DELETE FROM Difuzare d where d.filmDifuzat = :filmID")
                  .setParameter("filmID", film).executeUpdate();
    }

    public void stergeDifuzari(List<Difuzare> d){
        em.remove(d);
    }
}
