
package dao;

import db.Film;
import db.Sala;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;


public class SalaDao {
    
    private EntityManager em;
    
    public SalaDao (EntityManager em){
        this.em = em;
    }
    
    public List<Sala> getSali(){
        String sql = "SELECT s FROM Sala s";
        TypedQuery<Sala> query = em.createQuery(sql, Sala.class);
        return query.getResultList();
    }
    
    public void adaugaSala(Sala s){
        em.persist(s);
    }
    
    public List<Sala> findSala(String n){
        String jpql = "SELECT s FROM Sala s WHERE s.nume = '" + n + "'";
        List<Sala> query = em.createQuery(jpql, Sala.class).getResultList();
        return query;
    }
    
}