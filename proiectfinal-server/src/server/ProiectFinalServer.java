
package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import service.MainService;

public class ProiectFinalServer {

    public static void main(String[] args) {
        try{
            Registry reg = LocateRegistry.createRegistry(7001);
            reg.rebind("server", new MainService());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
}
