
package service;

import dao.DifuzareDao;
import dao.FilmDao;
import dao.SalaDao;
import dao.SpectatorDao;
import db.Difuzare;
import db.Film;
import db.Sala;
import db.Spectator;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import rmi.RemoteService;

public class MainService extends UnicastRemoteObject implements RemoteService {
    
    private EntityManagerFactory emf;
    
    public MainService() throws RemoteException{
        emf = Persistence.createEntityManagerFactory("proiectfinal-serverPU");
        try{
            Sala s1 = new Sala();
            s1.setNumarLocuri(50);
            s1.setNume("SmallOne");
            this.adaugaSala(s1);
            Sala s2 = new Sala();
            s2.setNumarLocuri(100);
            s2.setNume("BigOne");
            this.adaugaSala(s2);
            Sala s3 = new Sala();
            s3.setNumarLocuri(75);
            s3.setNume("MediumOne");
            this.adaugaSala(s3);
        }catch(Exception exception) {
            System.out.println("Salile deja introduse in baza de date");
        }
    }
    
    @Override
    public List<Film> getFilme(){
        EntityManager em = emf.createEntityManager();
        FilmDao filmDao = new FilmDao(em);
        return filmDao.getFilme();
    }
    
    @Override
    public void adaugaFilm(Film f){
        EntityManager em = emf.createEntityManager();
        FilmDao filmDao = new FilmDao(em);
        try{
            em.getTransaction().begin();
            filmDao.adaugaFilm(f);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    @Override
    public List<Film> gasesteFilm(String titlu){
        EntityManager em = emf.createEntityManager();
        FilmDao filmDao = new FilmDao(em);
        return filmDao.gasesteFilm(titlu);
    }
    
    
    @Override
    public void findAndDeleteFilm(String titlu){
        EntityManager em = emf.createEntityManager();
        String jpql = "SELECT f FROM Film f WHERE f.titlu = '" + titlu + "'";
        Film film = em.createQuery(jpql, Film.class).getSingleResult();
        FilmDao filmDao = new FilmDao(em);
        DifuzareDao difuzareDao = new DifuzareDao(em);
        try{
            em.getTransaction().begin();
            difuzareDao.stergeDifuzariDupaFilm(film);
            filmDao.stergeFilm(film);
            em.getTransaction().commit();
            }
        finally{
            em.close();
        }
    }
    
    @Override
    public List<Difuzare> getDifuzari(){
        EntityManager em = emf.createEntityManager();
        DifuzareDao difuzareDao = new DifuzareDao(em);
        return difuzareDao.getDifuzari();
    }
    
    @Override
    public void adaugaDifuzare(Difuzare d){
        EntityManager em = emf.createEntityManager();
        DifuzareDao difuzareDao = new DifuzareDao(em);
        try{
            em.getTransaction().begin();
            difuzareDao.adaugaDifuzare(d);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }   
    
    @Override
    public Difuzare getDifuzare(int d){
        EntityManager em = emf.createEntityManager();
        DifuzareDao difuzareDao = new DifuzareDao(em);
        Difuzare found = difuzareDao.getDifuzare(d);
        return found;
    }
    
    @Override
    public Difuzare gasesteDifuzare(Date data, Sala sala){
        EntityManager em = emf.createEntityManager();
        DifuzareDao difuzareDao = new DifuzareDao(em);
        return difuzareDao.gasesteDifuzare(data, sala);
    }
    
    @Override
    public List<Spectator> getSpectatori(){
        EntityManager em = emf.createEntityManager();
        SpectatorDao spectatorDao = new SpectatorDao(em);
        return spectatorDao.getSpectatori();
    }
    
    @Override
    public void adaugaSpectator(Difuzare d){
        EntityManager em = emf.createEntityManager();
        DifuzareDao difuzareDao = new DifuzareDao(em);
        try{
            em.getTransaction().begin();
            difuzareDao.adaugaSpectator(d);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }   
    
    @Override
    public List<Sala> getSali(){
        EntityManager em = emf.createEntityManager();
        SalaDao salaDao = new SalaDao(em);
        return salaDao.getSali();
    }
    
    @Override
    public void adaugaSala(Sala s) {
        EntityManager em = emf.createEntityManager();
        SalaDao salaDao = new SalaDao(em);
        try{
            em.getTransaction().begin();
            salaDao.adaugaSala(s);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    @Override
    public List<Sala> findSala(String n){
        EntityManager em = emf.createEntityManager();
        SalaDao salaDao = new SalaDao(em);
        return salaDao.findSala(n);
    }
}
